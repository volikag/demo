﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
namespace RLCLab
{
    public class Bill
    {
        private List<Item> _items;
        private Customer _customer;
        public Bill(Customer customer)
        {
            this._customer = customer;
            this._items = new List<Item>();
        }
        public void addGoods(Item arg)
        {
            _items.Add(arg);
        }
        public String statement()
        {
            double totalAmount = 0;
            int totalBonus = 0;
            List<Item>.Enumerator items = _items.GetEnumerator();
            String result = GetHeader();

            while (items.MoveNext())
            {
                double thisAmount = 0;
                Item each = (Item)items.Current;
                //определить сумму для каждой строки
                double discount = GetDiscount(each);
                int bonus = GetBonus(each);
                // сумма
                thisAmount = GetSum(each);
                // используем бонусы
                if ((each.getGoods().getPriceCode() ==
                Goods.REGULAR) && each.getQuantity() > 5)
                    discount +=
                    _customer.useBonus((int)(GetSum(each)));
                if ((each.getGoods().getPriceCode() ==
                Goods.SPECIAL_OFFER) && each.getQuantity() > 1)
                    discount =
                    _customer.useBonus((int)(GetSum(each)));
                // учитываем скидку
                thisAmount =
                GetSum(each) - discount;
                //показать результаты
                result += GetItemString(thisAmount, each, discount, bonus);
                totalAmount += thisAmount;
                totalBonus += bonus;
            }
            //добавить нижний колонтитул
            result += "Сумма счета составляет " +
            totalAmount.ToString() + "\n";
            result += "Вы заработали " +
            totalBonus.ToString() + " бонусных балов";
            //Запомнить бонус клиента
            _customer.receiveBonus(totalBonus);
            return result;
        }

        private static string GetItemString(double thisAmount, Item each, double discount, int bonus)
        {
            string result = "\t" + each.getGoods().getTitle() + "\t" +

        "\t" + each.getPrice() + "\t" + each.getQuantity() +
        "\t" + (GetSum(each)).ToString(CultureInfo.InvariantCulture) +
        "\t" + discount.ToString(CultureInfo.InvariantCulture) + "\t" + thisAmount.ToString(CultureInfo.InvariantCulture) +
        "\t" + bonus.ToString(CultureInfo.InvariantCulture) + "\n";
            return result;
        }

        private static double GetSum(Item each)
        {
            return each.getQuantity() * each.getPrice();
        }

        private static double GetDiscount(Item each)
        {
            double discount = 0.0;
            switch (each.getGoods().getPriceCode())
            {
                case Goods.REGULAR:
                    if (each.getQuantity() > 2)
                        discount =
                        (GetSum(each)) * 0.03; // 3%
                    break;
                case Goods.SPECIAL_OFFER:
                    if (each.getQuantity() > 10)
                        discount =
                        (GetSum(each)) * 0.005; // 0.5%
                    break;
                case Goods.SALE:
                    if (each.getQuantity() > 3)
                        discount =
                        (GetSum(each)) * 0.01; // 0.1%
                    break;
            }
            return discount;
        }

        private static int GetBonus(Item each)
        {
            int bonus = 0;
            switch (each.getGoods().getPriceCode())
            {
                case Goods.REGULAR:
                   bonus =
                    (int)(GetSum(each) * 0.05);
                    break;
                case Goods.SALE:
                    bonus =
                    (int)(GetSum(each) * 0.01);
                    break;
            }

            return bonus;
        }

        private string GetHeader()
        {
            string result = "Счет для " + _customer.getName() + "\n";

            result += "\t" + "Название" + "\t" + "Цена" +
            "\t" + "Кол-во" + "Стоимость" + "\t" + "Скидка" +
            "\t" + "Сумма" + "\t" + "Бонус" + "\n";

            return result;
        }
    }
}