﻿using System;

using NUnit.Framework;

using RLCLab;

namespace RLCLabTests
{
    [TestFixture]
    public class RLCLabTest
    {
        [Test]
        public void DemoTest()
        {
            //Arrange
            Goods cola = new Goods("Cola", Goods.REGULAR);
            Goods pepsi = new Goods("Pepsi", Goods.SALE);
            Item i1 = new Item(cola, 6, 65);
            Item i2 = new Item(pepsi, 3, 50);
            Customer x = new Customer("test", 10);
            Bill b1 = new Bill(x);
            b1.addGoods(i1);
            b1.addGoods(i2);

            //Act
            string actual = b1.statement();

            //assert
            Assert.AreEqual("Счет для test\n\tНазвание\tЦена\tКол-воСтоимость\tСкидка\tСумма\tБонус\n\tCola\t\t65\t6\t390\t21.7\t368.3\t19\n\tPepsi\t\t50\t3\t150\t0\t150\t1\nСумма счета составляет 518.3\nВы заработали 20 бонусных балов", actual);
        }
        
        [Test]
        public void RegularWithDiscount()
        {
            //Arrange
            Goods cola = new Goods("Cola", Goods.REGULAR);
            Item i1 = new Item(cola, 3, 65);
            Customer customer = new Customer("test", 10);
            Bill b1 = new Bill(customer);
            b1.addGoods(i1);

            //Act
            string actual = b1.statement();

            //assert
            Assert.AreEqual("Счет для test\n	Название	Цена	Кол-воСтоимость	Скидка	Сумма	Бонус\n	Cola		65	3	195	5.85	189.15	9\nСумма счета составляет 189.15\nВы заработали 9 бонусных балов", actual);
        }
    }
}
